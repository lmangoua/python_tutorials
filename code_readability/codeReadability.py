# This is an example Python program.
# The user inputs a number.
# The program then outputs all the EVEN numbers from 0 to that number.
rangeNum = int(input("Enter the max number you'd like to go up to: \n"))
print("Print even numbers in that range: \n")
for i in range (0, rangeNum):
  if i%2 == 0:
    print(i)

strEntered = input("Enter a string: ")
reverseString = strEntered[::-1]
print("\nEntered string is: " + strEntered)
print("Reverse string is: " + reverseString)