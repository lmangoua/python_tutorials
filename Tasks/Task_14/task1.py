# Prompt user to enter a number
num = int(input("Please enter a number: "))

# Using the for loop, the program displays the sum of the number multiplied by numbers ranging from 1 till 12
for y in range(1, 13):
    print(str(num) + "*" + str(y) + "=" + str(num * y))
    print(" ")
