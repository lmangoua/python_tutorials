# Declare a variable and assign values
num = 20
i = 0

# Using a while loop, print numbers in descending order
print("Numbers in descending order from 20 are:")
while num >= 0:
    print(num)
    num -= 1

# Declare variables and assign values to it
num = 20
i = 1

# Using the for loop, print out the even numbers between 0 and 20
print("Even numbers between 0 and 20 are:")
for i in range(1, num):
    if i % 2 == 0:
        print(i)
        i += 2

# Declare variables and assign values to it
print("Printing Stars in a specified sequence")
num1 = 0
stars = ""

# Using the loop feature, create loop to display a specified pattern
for num1 in range(0, 5):
    stars += "*"
    print(stars)

print("GCD (highest common factor) of two positive integers")

# Prompt user to enter two positive integers
num1 = int(input("Enter a positive number: "))
num2 = int(input("Enter another positive number: "))

# Declare variable to hold the GCD and set counter to 1
gcd = 0
i = 1

# Using a while loop calculate the GCD using an if statement
while i <= num1 and i <= num2:
    if num1 % i == 0 and num2 % i == 0:
        gcd = i
    i = i + 1

# Print GCD
print("\n HCF of {0} and {1} = {2}".format(num1, num2, gcd))
