import math

expression1 = (5 + 6) * 2
print("expression1 is: " + str(expression1))

expression2 = 6 * 4 - 10 / 5 * 2
print("expression2 is: " + str(expression2))

expression3 = ((15 + 5) - 10) / 2
print("expression3 is: " + str(expression3) + "\n")

weight = float(input("Enter your weight: "))
height = float(input("Enter your height: "))
bmi = weight + (height * height)
bmi1 = weight + (height ** 2)
bmi2 = weight + math.pow(height, 2)
print("Your BMI is " + str(bmi))
print("Your BMI1 is " + str(bmi1))
print("Your BMI2 is " + str((bmi2)))